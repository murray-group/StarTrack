#!/usr/bin/python

import numpy as np
import random
import math
import os


def load_txt_file(file_name,dtype=int):
    data = []
    with open(file_name,'r') as reader:
        content = reader.readlines()
    for t,line in enumerate(content):
        line = line.rstrip('\n')
        entries = line.split('\t')
        if t > 0:
            data.append(entries)
    data = np.asarray(data,dtype)
    return data   

def write_csv(cm,path_out):
    with open(path_out,'w') as writer:
        for i in range(cm.shape[0]):
            for j in range(cm.shape[1]):
                writer.write(str(cm[i,j]))
                if j != cm.shape[1]-1:
                    writer.write(',')
            if i != cm.shape[0]-1:
                writer.write('\n')
                
def prob_distance(x,D,t):
    return np.exp(-np.power(x,2)/(4*D*t))
           
    
def calculate_cost_matrix(data,D,false_negative,threshold_possible_connections,last_frame):
    
    n_points = data.shape[0]
    cost_matrix = np.ndarray(shape=(n_points+2,n_points+2), dtype=float)
    cost_matrix[:,:] = np.nan
    for i in range(n_points):
        for j in range(n_points):
            x = np.sqrt(np.power(data[i,0]-data[j,0],2) + np.power(data[i,1]-data[j,1],2))
            t = data[i,2]-data[j,2]
            if t > 0:
                cost_matrix[i,j] = prob_distance(x,D,t)*np.power(false_negative,t-1)
        
    cost_matrix[n_points+1,:] = threshold_possible_connections
    cost_matrix[n_points,:n_points] = prob_distance(0,D,last_frame-data[:,2]+1)*np.power(false_negative,last_frame-data[:,2])
    cost_matrix[:n_points,n_points] = prob_distance(0,D,data[:,2])*np.power(false_negative,data[:,2]-1)
    cost_matrix_filtered = cost_matrix.copy()
    cost_matrix_filtered[cost_matrix_filtered<threshold_possible_connections] = np.nan
    cost_matrix = -np.log(cost_matrix)
    cost_matrix_filtered = -np.log(cost_matrix_filtered)
    return [cost_matrix,cost_matrix_filtered]
      
    
D = 0.0002/np.power(0.0668,2)*60;
false_negative = 0.0822;
false_positive = 0.0244;
frame_skips = 3;
cmax = 0.0001;
thresh_possible_connections = np.power(false_negative,(frame_skips+1))
thresh_possible_connections = max(cmax,thresh_possible_connections)


data = load_txt_file("points.csv",dtype=float)
last_frame = data[-1,-1]
[cm,cmf] = calculate_cost_matrix(data,D,false_negative,thresh_possible_connections,last_frame)
write_csv(cmf,'cm.csv')
command = "./AStarTracking -path cm.csv -path_out tracked.txt";
os.system(command)





class Point:
    def __init__(self, index, y, x, t):
        self.index = index
        self.x = x
        self.y = y
        self.t = t
        self.is_special = False
        self.is_connected = False
        self.incoming = None;
        self.outgoing = [];
        
    '''
    def __init__(self,t):
        self.index = -1
        self.x = np.Nan
        self.y = np.Nan
        self.t = t
        self.special = true
        self.incoming = None;
        self.outgoing = [];
    '''  
    def get_incoming(self):
        if self.incoming != None:
            return self.incoming.earlier
        else:
            return None
        
    
    def has_connected_outgoing(self):
        for l in self.outgoing:
            if l.later.is_connected:
                return True
        return False
        
class Link:
    def __init__(self, earlier,later,cost):
        self.cost = cost
        if later.incoming == None:
            later.incoming = self
            self.earlier = earlier
            earlier.outgoing.append(self)
            self.later = later
        else:
            if later.is_special:
                later.incoming.append(self)
                self.earlier = earlier
                earlier.outgoing.append(self)
                self.later = later
            else:
                print("error: later point already has incoming link (point :",later.index,")")
                return
        
        if later.is_connected:
            current = earlier
            while not current.is_special and not current.is_connected:
                current.is_connected = True
                current = current.get_incoming()
                
                
    def delete(self):
        b = False
        for index,o in enumerate(self.earlier.outgoing):
            if o==self:
                b = True
                break
            
        if not b:
            print('something went wrong removing the outgoing link')
            return
        
        del self.earlier.outgoing[index]
        
        if self.later.is_special:
            b = False
            for index,i in enumerate(self.later.incoming):
                if i==self:
                    b = True
                    break
            if not b:
                print('something went wrong removing the incoming link in the special case')
                return
            
            del self.later.incoming[index]
        else:
            self.later.incoming = None

        current = self.earlier
        while not current.is_special and current.is_connected and not current.has_connected_outgoing():
            current.is_connected = False
            current = current.get_incoming()

            


def calculate_tracking_cost(points,point_end,fp_cost,cost_rp):
    cost = 0
    for i in range(len(points)):
        p = points[i]
        if p.is_connected:
            cost+=p.incoming.cost+cost_rp
        else:
            cost+=fp_cost
    
    for l in point_end.incoming:
        cost+=l.cost+cost_rp
    
    return cost

links = load_txt_file('tracked.txt')
points = [];
for i in range(len(data)):
    points.append(Point(i,data[i,0],data[i,1],data[i,2]-1))
    
point_start = Point(-1,0,0,0)
point_start.is_special = True
point_end = Point(-2,0,0,0)
point_end.is_connected = True
point_end.is_special = True
point_end.is_connected = True
point_end.incoming = []

n_points = len(points)
for i in range(links.shape[0]):
    p1 = links[i,0]-1
    p2 = links[i,1]-1
    if p2<n_points:
        #print(cm[p2,p1])
        Link(points[p1],points[p2],cm[p2,p1])

#link each point to an earlier point
for i in range(n_points):
    if points[i].incoming == None:
        #print(cm[i,:])
        tmp = np.nanargmin(cm[i,:])
        #print(tmp)
        cost = cm[i,tmp]
        if tmp < n_points:
            Link(points[tmp],points[i],cost)
        else:
            Link(point_start,points[i],cost)  
            
             
cost_fp = -np.log(false_positive)    
cost_rp = -np.log(1-false_positive)

#link try to interweave loose ends
for it in range(200):
    loose_ends = []
    switch = False;
    for i in range(n_points):
        if len(points[i].outgoing) == 0:
            loose_ends.append(i)
    
    for i in reversed(loose_ends):
        #print('current loose end',i)
        le = points[i]
        costs = []
        point_index = []
        cost_og = calculate_tracking_cost(points,point_end,cost_fp,cost_rp)
        for j in range(i+1,n_points):
            pl = points[j]
            if pl.t>le.t:
                pe = pl.get_incoming()
                pl.incoming.delete()
                Link(le,pl,cm[j,i])
                costs.append(calculate_tracking_cost(points,point_end,cost_fp,cost_rp))   
                point_index.append(j)
                pl.incoming.delete()
                Link(pe,pl,cm[j,pe.index])
        
        l = Link(le,point_end,cm[n_points,i])
        costs.append(calculate_tracking_cost(points,point_end,cost_fp,cost_rp))   
        l.delete()
            
        mc = min(costs)
        if mc < cost_og:
            index = [a for a, b in enumerate(costs) if b == mc]
            index = index[0]
            if len(costs)-1==index:
                #print('went to end')
                Link(le,point_end,cm[n_points,i])
            else:
                #print('replaced other edge')
                index = point_index[index]
                points[index].incoming.delete()
                Link(le,points[index],cm[index,i])
            switch = True
            break
            
            
    if not switch:
        break

#shuffle links
#  a----b
#   \  /
#     X
#   /  \
#  c----d
#-----current
#/X\ new
for it in range(200):
    switch = False;
    for i in range(n_points):
        cost_og = calculate_tracking_cost(points,point_end,cost_fp,cost_rp)
        costs = []
        point_index = []
        b = points[i]
        a = b.get_incoming()
        for j in range(i+1,n_points):
            d = points[j]
            c = d.get_incoming()
            if not a.is_special and not c.is_special:
                if a.t<d.t and c.t < b.t:
                    #print([a.index,b.index,c.index,d.index])
                    b.incoming.delete()
                    d.incoming.delete()
                    Link(a,d,cm[d.index,a.index])
                    Link(c,b,cm[b.index,c.index])
                    costs.append(calculate_tracking_cost(points,point_end,cost_fp,cost_rp))
                    point_index.append(j)
                    b.incoming.delete()
                    d.incoming.delete()
                    Link(a,b,cm[b.index,a.index])
                    Link(c,d,cm[d.index,c.index])

        if len(costs)>0:
            mc = min(costs)
            if mc < cost_og:
                index = [n for n, m in enumerate(costs) if m == mc]
                index = point_index[index[0]]
                d = points[index]
                c = d.get_incoming()
                b.incoming.delete()
                d.incoming.delete()
                Link(a,d,cm[d.index,a.index])
                Link(c,b,cm[b.index,c.index])
                switch = True
                
    if not switch:
        break

used_points = []
for i in range(n_points):
    used_points.append(False)
    
tras = []
for i in reversed(range(n_points)):
    p = points[i]
    if p.is_connected and (not used_points[i]):
        tra = []
        cp = p
        while True:
            tra.append(cp)
            cp = cp.incoming.earlier
            if cp.is_special:
                break
            if used_points[cp.index]:
                break
            used_points[cp.index] = True
        if len(tra) > 0:
            tras.append(tra)
            
n_tra = len(tras)
trajectory_matrix = np.ndarray([int(last_frame),n_tra*2],dtype=float)
trajectory_matrix[:,:] = np.nan
for i in range(n_tra):
    tra = tras[i]
    for j in reversed(range(len(tra))):
        p = tra[j]
        t = int(p.t)
        trajectory_matrix[t,i]=p.x;
        trajectory_matrix[t,i+n_tra]=p.y;
write_csv(trajectory_matrix,"TrajectoryMatrix.csv")
