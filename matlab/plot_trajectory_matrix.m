function plot_trajectory_matrix(trajectory_matrix)
    for i = 1:size(trajectory_matrix,2)
        y = trajectory_matrix(:,i,1);
        x = 1:numel(y);
        x = x(~isnan(y));
        y = y(~isnan(y));
        plot(x,y,'-x')
        if ~ishold, hold on,end
    end
    hold off
    xlabel('Frame')
    ylabel('Position')
end