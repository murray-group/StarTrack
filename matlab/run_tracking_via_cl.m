D = 2e-4/0.0668^2*60;%diffusion in pixel^2 per minute
false_negative = 0.0822;
false_positive = 0.0244;
frame_skips = 3;%number of maximum frame skips
cmax = 0.0001;%this is the probability at which a connection is deemed to improbable to be true and wont be considred further
thresh_possible_connections = false_negative^(frame_skips+1);
thresh_possible_connections = max(cmax,thresh_possible_connections);


cycle = load("example_cycle.mat");
cycle = cycle.cycle;


positions = cycle.foci1.position;%tracking will be done on these positions
positions_original = positions;%however these positions will be the one returned at the end
cycle_lengths = cycle.lengths;%length vector can be passed to correct for growth

figure(1)
subplot(1,2,1)
plot(positions(:,:,1),'x')
title("Raw Data")
xlabel("Frame")
ylabel("Position (Pixel)")

[cost_matrix_filtered,cost_matrix,foci_data] = create_cost_matrix(positions,D,thresh_possible_connections ...
    ,false_negative,cycle.duration,positions_original,cycle_lengths);
csvwrite('cm.csv',cost_matrix_filtered);%create cost matrix to use with cpp
command = "cpp/./AStarTracking -path cm.csv -path_out tracked.txt -elements 100000";%command to run the cpp code
[status,cmdout] = system(command);%execute the command
edges = load_edges("tracked.txt");%load the tracking produced by the A* algorithm
[trajectory_matrix,start_and_stop] = stitching(edges,cost_matrix,foci_data,false_positive,cycle.duration);%stitch together the contigous trajectories

subplot(1,2,2)
plot_trajectory_matrix(trajectory_matrix)
title("Tacked Data")
xlabel("Frame")
ylabel("Position (Pixel)")


function edges = load_edges(path)
    tracking = tdfread(path,"\t");
    fn = fieldnames(tracking);
    tmp = cell(1,numel(fn));
    for i = 1:numel(fn)
        tmp{i} = tracking.(fn{i});
    end
    edges = cat(2,tmp{:});
end


%%
