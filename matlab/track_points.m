function [trajectory_matrix_inferred,edges,fd] = track_points(point_matrix,D,fn,fp,fs,cmax,run_stitching,max_candidate_list_size)
    thresh_possible_connections = fn^(fs+1);
    thresh_possible_connections = max(cmax,thresh_possible_connections);
    [cmf,cm,fd] = create_cost_matrix(point_matrix,D,thresh_possible_connections ...
            ,fn,size(point_matrix,1),point_matrix);   

    edges = trackingAstar(cmf,max_candidate_list_size,0.9,5);
    if run_stitching
        [trajectory_matrix_inferred,start_and_stop] = stitching(edges,cm,fd,fp,size(point_matrix,1));
    else
        [trajectory_matrix_inferred] = edges_to_matrix(edges,fd);
    end
    
end

function [trajectory_matrix_inferred] = edges_to_matrix(edges,foci_data)%convert the edges between foci into a trajctory matrix
    tracked_points = false(1,size(edges,1));
    trajectories = cell(0,1);
    for p_it = 1:numel(tracked_points)
        if(~tracked_points(p_it))
            trajectory = rec1(edges,p_it);
            trajectories{end+1} = trajectory;
            tracked_points(trajectory) = true;
        end
    end
    trajectory_matrix_inferred = nan(max(foci_data(:,4)),numel(trajectories),2);
    for t_it = 1:numel(trajectories)
        trajectory = trajectories{t_it};
        for j = 1:numel(trajectory)
            t = foci_data(trajectory(j),[1,2,4]);
            trajectory_matrix_inferred(t(3),t_it,:) = t([1,2]);
        end
    end
end



function [tra] = rec1(edges,index)
    entry = edges(index,:);
    tra = index;
    if(entry(2)<=size(edges,1))
        tra = [tra,rec1(edges,entry(2))];
    end
end
