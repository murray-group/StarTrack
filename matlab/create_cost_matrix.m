%[cmf,cm,fd] = create_cost_matrix(cycle.foci1.position(1:3,:,:),D,thresh_possible_connections,false_negative,3,cycle.foci1.position(1:3,:,:),[1,2,3]);
function [cost_matrix_filtered,cost_matrix,foci_data] = create_cost_matrix(position,D,thresh_possible_connections,false_negative,last_frame,position_original,l)
    %thresh_possible_connections = thresh_possible_connections*(1-false_positive);%multiply thresh by 1-fp because the same thing is done with all links
    weights = ones(size(position,[1,2]));
    frame_number = repmat((1:size(position,1))',[1,size(position,2)]);%get the frame number for each foci
    foci_data = cat(3,position,weights,frame_number,position_original);%combine all information (position,intensity,frame number)
    foci_data = permute(foci_data,[2,1,3]);%permute foci data so points are sorted by frame number once foci data is linearized
    foci_data = reshape(foci_data,[size(position,1)*size(position,2),6]);%linearize data so we have a linear list of points each having 4 characteristics (see above)
    foci_data = foci_data(~isnan(foci_data(:,1)),:);%remove empty entries which exist due to the way data is stored in cycle.foci
    n_foci = size(foci_data,1);%number of foci
    tmp1 = reshape(foci_data,[size(foci_data,1),1,size(foci_data,2)]);%create to list of foci_data which different shapes
    tmp2 = reshape(foci_data,[1,size(foci_data,1),size(foci_data,2)]);%to create a matrix with the differences between all points
    foci_pairs = tmp1-tmp2;%matrix with differences of all pairs of points
    foci_pairs(:,:,3) = (tmp1(:,:,3)+tmp2(:,:,3))/2;%set the weighting of all the foci pairs
    
    %foci_pairs
    if exist('l','var')
        distance_matrix = nan(n_foci,n_foci);
        for i = 1:n_foci
            for j = 1:n_foci
                distance_matrix(i,j) = sqrt((foci_data(i,1)-foci_data(j,1)*l(foci_data(i,4))/l(foci_data(j,4)))^2+(foci_data(i,2)-foci_data(j,2))^2);
            end
        end
    else
        distance_matrix = sqrt(foci_pairs(:,:,1).^2+foci_pairs(:,:,2).^2);%distance between all foci pairs
    end
    %distance_matrix
    %a pair of points represents an edge. Multiple connected edges will form a trajectory
    
    
    probDistance = @(x,D,t) exp(-x.^2./(4*D*t));%./(4*pi*D*t);%function of how probable a certain distance given a diffusion rate is
    
    cost_matrix = probDistance(distance_matrix,D,foci_pairs(:,:,4)).*false_negative.^(foci_pairs(:,:,4)-1);%*(1-false_positive);%the cost of each pair of foci being adjacent inside a trajectory. this cost will be minimized by this algorithm
    cost_matrix = cost_matrix.*foci_pairs(:,:,3);
    cost_matrix(foci_pairs(:,:,4)<=0) = nan;%set all edges to zeros which point back in time (from a later frame to an earlier one)
    tmp = eye(n_foci); %remove the diagonal from the cost matrix
    cost_matrix(tmp==1) = nan;
    %m = median(min(cost_matrix,[],'omitnan'),'omitnan')
    %m = median(cost_matrix(:),'omitnan');
    
    cost_matrix = -log(cost_matrix);
    
    cost_matrix(:,size(cost_matrix,2)+1) = nan;%set skip to the end positives
    cost_matrix(size(cost_matrix,1)+1,:) = nan;
    skipping_to_last_frame = -(log(false_negative.^(last_frame-foci_data(:,4)))+log(probDistance(0,D,(last_frame-foci_data(:,4))+1)));
    skipping_to_first_frame = -(log(false_negative.^(foci_data(:,4)-1))+log(probDistance(0,D,foci_data(:,4))));%*(1-false_positive)));
    cost_matrix(size(cost_matrix,1),:) = [skipping_to_last_frame;nan];
    cost_matrix(:,size(cost_matrix,2)) = [skipping_to_first_frame;nan];
    
    cost_matrix(:,size(cost_matrix,2)+1) = nan;%set false positives
    cost_matrix(size(cost_matrix,1)+1,:) = -log(thresh_possible_connections);
    cost_matrix(~isfinite(cost_matrix)) = nan; %get rid of infinity
    cost_matrix_filtered = cost_matrix;
    cost_matrix_filtered(cost_matrix_filtered>-log(thresh_possible_connections)) = nan;
    
end
