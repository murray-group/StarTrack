
function [trajectory_matrix,start_and_stop,graph] = stitching(edges,cost_matrix,foci_data,false_positive,n_frames)
%stitching takes a set of edges (created by trackingAstart) and a unfiltered cost matrix (created by 
%create_cost_matrix) to stitch the trajectories into one coherened tracking (number of points does 
%not decrease. This is done by converting all edges into a symmetric directed graphs (implemented 
%using structs). each node has incoming and outgoing edges which connect each point to one or less
%points on a previous frame (incoming) and to zero or more points on subsequential frames.
%In a first step the beginning of each trajectory will be connected to its closest point or if a 
%trajectory is sufficently close to the first frame it can also go to a hypothetical point. Such a 
%connection implies that the trajectory started before for the first frame. After this step each
%lose end is identified. Lose ends are graph which do not habe any outgoing connection. Similarly to
%points at the first frames, points on the later frames can be connected to the hypothetical point.
%This implies that the trajectory continues after the last frame. These points are not lose ends.
%Lose ends can be interweaved into the tracking by replacing a node in an existing connection. This
%way the lose end stops being a lose end, however the node which was replaced loses an outgoing edge
%and might become itself a lose end. Therefore a lose end is only interweaved into the tracking if
%it imporves the overall cost/probability of the tracking. After all lose ends werre considered for
%the act of interweaving, one last thing remains. Each edge can be shuffled with another edge
%(replacing incoming and outgoing connections of the graph involved). Each edge is tested if it can
%be shuffled with any other edge to improve the overall cost of the tracking. If this is the case
%the edges are shuffled. This process continues untill there are no shuffle events left which
%improve the tracking.

%one key information is that the hypothetical points have the index: "number of graph + 1/2/3".
%also connection and edges are the same thing. I wasnt consistent with the naming but ok.n_nodes = size(cost_matrix,1)-2;%number of points which are present in the tracking
    if(false_positive<=0)%a false postive rate of 0 causes the cost of false positives to be infinite. To avoid this issue false positive is capped at 0.000001
        false_positive=0.00000001;
    end

    cost_matrix = cost_matrix+log(1-false_positive);%this is done to take account of each link implying the existence of a real foci
    tmp = cost_matrix(:,end);
    cost_matrix(:,end) = cost_matrix(:,end-1);
    cost_matrix(:,end-1) = tmp;


    n_nodes = size(cost_matrix,1)-2;%number of points which are present in the tracking
    [graph] = create_graph(n_nodes,false_positive);
    
    for edge_it = 1:size(edges,1)%add all the edges to the graph of graph
        edge = edges(edge_it,:);
        %if edge(2)==n_nodes+2,edge(2)=edge(2)+1;end%to map the false positive candidates to n_nodes+3
        if edge(2)>n_nodes,edge(2)=n_nodes+3;end%this tests all foci is they are benefitial to the tracking and does not give foci with where identified by A* to end a free pass
        graph = add_edge(graph,edge,cost_matrix);
        %edges(edge_it,:) = edge;
    end

    debug = false;
    if debug
        figure(4)
        subplot(4,1,1)
        visualize_graph(graph,foci_data);
        title('initial')
    end
    
    %loose_ends = find_loose_ends(graph);
    %manipulate the cost matrix so it encourages connections to lose ends
    cost_matrix_manipulated = cost_matrix;
    %cost_matrix_manipulated(1:n_nodes,loose_ends) = cost_matrix_manipulated(1:n_nodes,loose_ends)--log(fp);

    
    %connect all lose beginning to their closest point
    loose_beginnings = find_loose_beginnings(graph);
    for lb_index = loose_beginnings
        [~,cf_index] = min(cost_matrix_manipulated(lb_index,:));%closest foci index
        graph = add_edge(graph,[cf_index,lb_index],cost_matrix);
    end
    
    if debug
        subplot(4,1,2)
        visualize_graph(graph,foci_data);
        title('connected beginnings')
    end
    
    %interweave
    repeat = true;
    while repeat
        repeat = false;
        loose_ends = find_loose_ends(graph);
        if numel(loose_ends)
            loose_ends = sort(loose_ends,'descend');
            for le_index = loose_ends
                [graph,interweaved] = interweave_loose_end(le_index,graph,cost_matrix,foci_data,false_positive,n_frames);
                repeat = repeat || interweaved;
            end
        end
    end
    
    if debug
        subplot(4,1,3)
        visualize_graph(graph,foci_data);
        title('interwoven')
    end
    %%{
    %shuffle
    repeat = true;
    %graph = switch_edges(57,62,58,60,graph,cost_matrix);
    while repeat
        [graph,repeat] = improve_graph(graph,cost_matrix,foci_data);
    end
    %}
    %figure(4)
    %visualize_graph(graph,foci_data);
    [trajectory_matrix,start_and_stop] = assemble_trajectories(graph,foci_data,n_frames);
    
    if debug
        subplot(4,1,4)
        visualize_graph(graph,foci_data);
        title('shuffled')
    end
end

function [graph,switched] = improve_graph(graph,cm,fd)
    switched = false;
    n_nodes = numel(graph)-3;
    for n12_index = 1:n_nodes
        delta_costs = nan(1,n_nodes);
        n11_index = graph(n12_index).incoming;
        for n22_index = n12_index+1:n_nodes
            %if ~graph(n12_index).false_positive && ~graph(n22_index).false_positive

                n21_index = graph(n22_index).incoming;
                %test = (cm(n12_index,n11_index)+cm(n22_index,n21_index))
                %[n12_index,n21_index]
                %[n11_index,n12_index,n21_index,n22_index]
                test2 = cm(n12_index,n21_index)+cm(n22_index,n11_index);
                test1 = cm(n12_index,n11_index)+cm(n22_index,n21_index);
                dc = (cm(n12_index,n21_index)+cm(n22_index,n11_index))-(cm(n12_index,n11_index)+cm(n22_index,n21_index));
                delta_costs(n22_index) = dc;
            %end
        end
        [delta_cost, n22_index] = min(delta_costs);
        if delta_cost < 0
            n21_index = graph(n22_index).incoming;
            graph = switch_edges(n11_index,n12_index,n21_index,n22_index,graph,cm);
            switched = true;
        end
    end
    %{
    switched = false;
    n_nodes = numel(graph)-3;
    graph_cost(graph)
    for n12_index = 1:n_nodes
        costs = nan(1,n_nodes);
        cost_base = graph_cost(graph);
        n11_index = graph(n12_index).incoming;
        for n22_index = n12_index:n_nodes
            n21_index = graph(n22_index).incoming;
            if n21_index < n12_index
                graph_tmp = switch_edges(n11_index,n12_index,n21_index,n22_index,graph,cm);
                costs(n22_index) = graph_cost(graph_tmp);
            end
        end
        
        [delta_cost, n22_index] = min(costs-cost_base);
        if delta_cost < 0
            n21_index = graph(n22_index).incoming;
            [n11_index,n12_index,n21_index,n22_index]
            graph = switch_edges(n11_index,n12_index,n21_index,n22_index,graph,cm);
            switched = true;
            break
        end
    end
    graph_cost(graph)
    %}
end

function [graph,interweaved] = interweave_loose_end(le_index,graph,cm,fd,fp,n_frames)
    interweaved = false;
    n_nodes = numel(graph)-3;
    costs = nan(1,n_nodes);
    cost_base = graph_cost(graph);
    %cost_base = graph_cost2(graph,fd,fp,fn,n_frames);
    f = find(fd(:,4) > fd(le_index,4),1,'first');
    if numel(f)
        for n2_index = f:n_nodes
            n1_index = graph(n2_index).incoming;
            graph_tmp = switch_edges(le_index,n_nodes+3,n1_index,n2_index,graph,cm);
            costs(n2_index) = graph_cost(graph_tmp);
            %costs(n2_index) = graph_cost2(graph_tmp,fd,fp,fn,n_frames);
        end
    end
    graph_tmp = add_edge(graph,[le_index,n_nodes+1],cm);
    costs(end+1) = graph_cost(graph_tmp);
    %costs(end+1) = graph_cost2(graph_tmp,fd,fp,fn,n_frames);
    
    [cost, sw_index] = min(costs-cost_base);
    if cost < 0
        if sw_index==n_nodes+1
            graph = add_edge(graph,[le_index,sw_index],cm);
        else
            graph = switch_edges(le_index,n_nodes+3,graph(sw_index).incoming,sw_index,graph,cm);
        end
        interweaved = true;
    end
    %graph_cost2(graph,fd,fp,fn,n_frames)
end


function [indices] = find_loose_ends(graph)
    count = 0;
    indices = zeros(0,1);
    for index = 1:numel(graph)-3
        if graph(index).outgoing(1) == numel(graph)
            count = count+1;
            indices(count) = index;
        end
    end
end

function [indices] = find_loose_beginnings(graph)
    count = 0;
    indices = zeros(0,1);
    for index = 1:numel(graph)-3
        if graph(index).incoming(1) == numel(graph)
            count = count+1;
            indices(count) = index;
        end
    end
end

function graph = switch_edges(n11_index,n12_index,n21_index,n22_index,graph,cm)
    n11 = graph(n11_index);
    n12 = graph(n12_index);
    n21 = graph(n21_index);
    n22 = graph(n22_index);
    n11.outgoing(n11.outgoing==n12_index)=n22_index;
    n21.outgoing(n21.outgoing==n22_index)=n12_index;
    if n22_index~=numel(graph)
        n22.incoming(n22.incoming==n21_index) = n11_index;
    end
    if n21_index~=numel(graph)
        n12.incoming(n12.incoming==n11_index) = n21_index;
    end
    graph(n11_index) = n11;
    graph(n12_index) = n12;
    graph(n21_index) = n21;
    graph(n22_index) = n22;
    
    graph = update_graph(n11_index,graph,cm);
    graph = update_graph(n21_index,graph,cm);
end

function [graph] = create_graph(n_nodes,false_positive_rate)
    node = struct('index',-1,'outgoing',n_nodes+3,'incoming',n_nodes+3,'false_positive',true,'begin',false,'end',false,'nowhere',false,'cost',-log(false_positive_rate));
    graph = node;
    for i = 1:n_nodes+3%create a array of graph each node represents on point
        %n_nodes+1 is a hypothetical foci at the end
        %n_nodes+2 is a hypothetical foci at the beginning (previously from the A* this would have been a candidate for a false positive)
        %n_nodes+3 is a hypothetical foci that points into the abyss (nowhere)
        node.index = i;
        graph(i) = node;
    end
    graph(n_nodes+1).end = true;
    graph(n_nodes+1).false_positive = true;
    graph(n_nodes+1).cost = 0;
    graph(n_nodes+2).begin = true;
    graph(n_nodes+2).false_positive = true;
    graph(n_nodes+2).cost = 0;
    graph(n_nodes+3).nowhere = true;
    graph(n_nodes+3).false_positive = true;
    graph(n_nodes+3).incoming = n_nodes+3;
    graph(n_nodes+3).outgoing = n_nodes+3;
end

function [graph] = add_edge(graph,edge,cm)

    n_nodes = numel(graph)-3;%minus two to account for hypothetical foci
    if any(edge>n_nodes+2) %edge points to hypothetical false positive node (do nothing)
        return
    end
    n1 = graph(edge(1));
    n2 = graph(edge(2));
    
    if n2.incoming(1) == n_nodes+3
        n2.incoming = edge(1);
    else
        n2.incoming(end+1) = edge(1);
    end
    
    if n1.outgoing(1) == n_nodes+3
        n1.outgoing = edge(2);
    else
        n1.outgoing(end+1) = edge(2);
    end
    
    graph(edge(1)) = n1;
    graph(edge(2)) = n2;
    graph = update_graph(edge(1),graph,cm);
end

function [graph] = update_graph(n_index,graph,cm)
    c_node = graph(n_index);
    l_graph = graph(c_node.outgoing);
    u_indices = c_node.outgoing;
    u_indices(end+1) = n_index;%even though it is now twice in the list. it is need when edges are switched
    
    if c_node.false_positive
        while c_node.false_positive && ~c_node.begin && ~c_node.nowhere && (~all([l_graph.false_positive]) || any([l_graph.end]))
            c_node.false_positive = false;
            u_indices(end+1) = c_node.index;
            graph(c_node.index) = c_node;
            c_node = graph(c_node.incoming);
            l_graph = graph(c_node.outgoing);
        end
    else
        while ~c_node.false_positive && ~c_node.begin && ~c_node.nowhere && all([l_graph.false_positive]) && ~any([l_graph.end])
            c_node.false_positive = true;
            u_indices(end+1) = c_node.index;
            graph(c_node.index) = c_node;
            c_node = graph(c_node.incoming);
            l_graph = graph(c_node.outgoing);
        end
    end
    
    n_nodes = numel(graph)-3;
    for u_index = u_indices
        node = graph(u_index);
        if node.false_positive || node.incoming == n_nodes+3
            node.cost = graph(numel(graph)).cost;%last not representing nowhere always has the cost of false positive
        else
            node.cost = cm(node.index,node.incoming);
        end
        graph(u_index) = node;
    end
    
    if graph(n_index).outgoing==n_nodes+1
        node_end = graph(n_nodes+1);
        for n_it = 1:numel(node_end.incoming)
            node_end.cost(n_it) = cm(n_nodes+1,node_end.incoming(n_it));
        end
        graph(n_nodes+1) = node_end;
    end
end

function cost = graph_cost(graph)
    cost = sum([graph(1:end-3).cost])+sum(graph(end-2).cost);
    %%{
    %cost = 0;
    %for n_index = 1:numel(graph)-3
    %    cost = cost+graph(n_index).cost;
    %end
    %}
end

function cost = graph_cost2(graph,fd,fp,fn,n_frames)
    %cost = sum([graph(1:end-3).cost])+sum(graph(end-2).cost);
    n_nodes = numel(graph)-3;
    nfn = 0;
    nfp = 0;
    for i = 1:n_nodes
        node = graph(i);
        if node.false_positive
            nfp = nfp + node.false_positive;
        else
            in = node.incoming;
            if in <= n_nodes
                nfn = nfn + fd(i,4)-fd(in,4)-1;
            else
                nfn = nfn + fd(i,4)-0-1;
            end
        end
    end
    for i = graph(end-2).incoming
        if i <=n_nodes
            nfn = nfn + n_frames-fd(i,4);
        end
    end
    cfp = -log(fp)*nfp;
    cfn = -log(fn)*nfn;
    cost = cfp+cfn;
end

function visualize_graph(graph,foci_data)
    n_nodes = numel(graph)-3;
    %cm2 = get(gcf,'Colormap');
    ncs = nice_color_scale(200);
    plot(nan,nan)
    for n_it = 1:n_nodes
        node = graph(n_it);
        x = zeros(1,2);
        y = zeros(1,2);
        x(2) = foci_data(node.index,4);
        y(2) = foci_data(node.index,1);
        if node.incoming<=n_nodes && node.incoming~=n_nodes+1
            node_prev = graph(node.incoming);
            x(1) = foci_data(node_prev.index,4);
            y(1) = foci_data(node_prev.index,1);
            if ~node.false_positive
                plot(x, y,'-x', "Color", ncs(floor(foci_data(node.index,4)/max(foci_data(:,4))*200),:))
            else
                plot(x, y,'-x', "Color",[0.9,0.9,0.9])
            end
        end
        txt = strcat("(",string(node.index),"/",string(node.cost),")");
        %text(x(2),y(2),txt,'FontSize',7);
        if ~ishold,hold on,end
    end
    hold off

end

function [trajectories_matrix,start_and_stop] = assemble_trajectories(graph,foci_data,n_frames)
    n_nodes = numel(graph)-3;
    trajectory_endings = sort(graph(end-2).incoming,'descend');%all ending of trajectories
    is_used = false(1,n_nodes);%collect all points which are part of a trajectory
    trajectory_endings = trajectory_endings(trajectory_endings<=n_nodes);%to catch the case there are no endings
    trajectories = cell(1,numel(trajectory_endings));
    for tra_it = 1:numel(trajectory_endings)%for each ending collect points (which were not collected yet) until you reach the start
        tra = n_nodes+1;
        current_index = trajectory_endings(tra_it);
        while ~is_used(current_index)
            is_used(current_index) = true;
            tra(end+1) = current_index;
            current_index = graph(current_index).incoming;
            if current_index == n_nodes+2
                break
            end
        end
        
        tra(end+1) = current_index;
            
        trajectories{tra_it} = flip(tra);
    end
    
    trajectories_matrix = nan(n_frames,numel(trajectories),2);%put all trajectories in a matrix
    start_and_stop = nan(numel(trajectories),2);%put all trajectories in a matrix
    for i = 1:numel(trajectories)%put all trajectories into one matrix
        trajectory = trajectories{i};
        if numel(trajectory) 

            if trajectory(1) == n_nodes+2
                trajectory = trajectory(2:end);%remove hypothetical first point
                start = 1;
            else
                start = foci_data(trajectory(1),4)+1;
            end
            if trajectory(end) == n_nodes+1
                stop = n_frames;
                trajectory = trajectory(1:end-1);%remove hypothetical last point
            else
                stop = foci_data(trajectory(end),4);
            end

            start_and_stop(i,:) = [start,stop];
            trajectories_matrix(foci_data(trajectory,4),i,:) = [foci_data(trajectory,5),foci_data(trajectory,6)];
        end
    end
end


function [colors] = nice_color_scale(n)
    c_dark = [40,50,80];
    c_light = [240,84,64];
    c_r = linspace(c_dark(1),c_light(1),n);
    c_g = linspace(c_dark(2),c_light(2),n);
    c_b = linspace(c_dark(3),c_light(3),n);
    colors = [c_r;c_g;c_b]'/255;
end

%%