%runs the tracking step of u-track (https://github.com/DanuserLab/u-track)
%code is based on the file scriptTrackGeneral.m from the before mentioned repository
%it was altered by Robin Koehler


function [trajectory_matrix,tracksFinal] = run_uTrack_tracking(position,varargin)
    
    %Default Parameters
    timeWindow = 6; %maximum allowed time gap (in frames) between a track segment end and a track segment start that allows linking them.
    minTrackLen = 2; %minimum length of track segments from linking to be used in gap closing.
    linearMotion = 0; %use linear motion Kalman filter.
    minSearchRadius = 6; %minimum allowed search radius. The search radius is calculated on the spot in the code given a feature's motion parameters. 
    maxSearchRadius = 6; %maximum allowed search radius. Again, if a feature's calculated search radius is larger than this maximum, it will be reduced ...
    brownStdMult = 3; %multiplication factor to calculate search radius from standard deviation.
    useLocalDensity = 1; %1 if you want to expand the search radius of isolated features in the linking (initial tracking) step.
    brownScaling = [0.25 0.01]; %power for scaling the Brownian search radius with time, before and after timeReachConfB (next parameter).
    lenForClassify = 5; %minimum track segment length to classify it as linear or random.
    useLocalDensity2 = 0; %1 if you want to expand the search radius of isolated features in the gap closing and merging/splitting step.
    linStdMult = 3;%*ones(gapCloseParam.timeWindow,1); %multiplication factor to calculate linear search radius from standard deviation.
    linScaling = [1 0.01]; %power for scaling the linear search radius with time (similar to brownScaling).
    maxAngleVV = 30; %maximum angle between the directions of motion of two tracks that allows linking them (and thus closing a gap).
    gapPenalty = 5; %penalty for increasing temporary disappearance time (disappearing for n frames gets a penalty of gapPenalty^(n-1)).
    gapExcludeMS = 1; %flag to allow gaps to exclude merges and splits
    strategyBD = -1; %strategy to calculate birth and death cost
    
    it = 1;
    while(true)
        if it>numel(varargin),break;end
        arg = varargin{it};
        c = class(arg);
        
        if strcmp(c,'string') || strcmp(c,'char')
            if strcmp(arg,'timeWindow')
                timeWindow=varargin{it+1};
            elseif strcmp(arg,'minTrackLen')
                minTrackLen=varargin{it+1};
            elseif strcmp(arg,'linearMotion')
                linearMotion=varargin{it+1};
            elseif strcmp(arg,'minSearchRadius')
                minSearchRadius=varargin{it+1};
            elseif strcmp(arg,'maxSearchRadius')
                maxSearchRadius=varargin{it+1};
            elseif strcmp(arg,'brownStdMult')
                brownStdMult=varargin{it+1};
            elseif strcmp(arg,'useLocalDensity')
                useLocalDensity=varargin{it+1};
            elseif strcmp(arg,'brownScaling')
                brownScaling=varargin{it+1};
            elseif strcmp(arg,'lenForClassify')
                lenForClassify=varargin{it+1};
            elseif strcmp(arg,'useLocalDensity2')
                useLocalDensity2=varargin{it+1};
            elseif strcmp(arg,'linStdMult')
                linStdMult=varargin{it+1};
            elseif strcmp(arg,'linScaling')
                linScaling=varargin{it+1};
            elseif strcmp(arg,'maxAngleVV')
                maxAngleVV=varargin{it+1};
            elseif strcmp(arg,'gapPenalty')
                gapPenalty=varargin{it+1};
            elseif strcmp(arg,'gapExcludeMS')
                gapExcludeMS=varargin{it+1};
            elseif strcmp(arg,'strategyBD')
                strategyBD=varargin{it+1};
            else
                disp(strcat(['Wrong input at pos ',num2str(it+1),':']))
                disp(arg)
                it=it-1;
            end
            it=it+1;
        else
            disp(strcat(['Wrong input at pos ',num2str(it+1),':']))
            disp(arg)
        end
        it=it+1;
    end
    
    

    gapCloseParam.timeWindow = timeWindow; %maximum allowed time gap (in frames) between a track segment end and a track segment start that allows linking them.
    gapCloseParam.mergeSplit = 3; %1 if merging and splitting are to be considered, 2 if only merging is to be considered, 3 if only splitting is to be considered, 0 if no merging or splitting are to be considered.
    gapCloseParam.minTrackLen = minTrackLen; %minimum length of track segments from linking to be used in gap closing.

    %optional input:
    gapCloseParam.diagnostics = 0; %1 to plot a histogram of gap lengths in the end; 0 or empty otherwise.

    % cost matrix for frame-to-frame linking

    %function name
    costMatrices(1).funcName = 'costMatRandomDirectedSwitchingMotionLink';

    %parameters

    parameters.linearMotion = linearMotion; %use linear motion Kalman filter.
    parameters.minSearchRadius = minSearchRadius; %minimum allowed search radius. The search radius is calculated on the spot in the code given a feature's motion parameters. If it happens to be smaller than this minimum, it will be increased to the minimum.
    parameters.maxSearchRadius = maxSearchRadius; %maximum allowed search radius. Again, if a feature's calculated search radius is larger than this maximum, it will be reduced to this maximum.
    parameters.brownStdMult = brownStdMult; %multiplication factor to calculate search radius from standard deviation.

    parameters.useLocalDensity = useLocalDensity; %1 if you want to expand the search radius of isolated features in the linking (initial tracking) step.
    parameters.nnWindow = gapCloseParam.timeWindow; %number of frames before the current one where you want to look to see a feature's nearest neighbor in order to decide how isolated it is (in the initial linking step).

    parameters.kalmanInitParam = []; %Kalman filter initialization parameters.
    % parameters.kalmanInitParam.searchRadiusFirstIteration = 10; %Kalman filter initialization parameters.

    %optional input
    parameters.diagnostics = []; %if you want to plot the histogram of linking distances up to certain frames, indicate their numbers; 0 or empty otherwise. Does not work for the first or last frame of a movie.

    costMatrices(1).parameters = parameters;
    clear parameters

    % cost matrix for gap closing

    %function name
    costMatrices(2).funcName = 'costMatRandomDirectedSwitchingMotionCloseGaps';
    %costMatrices(2).funcName = 'costMatRandomDirectedSwitchingMotionLink';

    %parameters

    %needed all the time
    parameters.linearMotion = linearMotion; %use linear motion Kalman filter.

    parameters.minSearchRadius = minSearchRadius; %minimum allowed search radius.
    parameters.maxSearchRadius = maxSearchRadius; %maximum allowed search radius.
    parameters.brownStdMult = brownStdMult*ones(gapCloseParam.timeWindow,1); %multiplication factor to calculate Brownian search radius from standard deviation.

    parameters.brownScaling = brownScaling; %power for scaling the Brownian search radius with time, before and after timeReachConfB (next parameter).
    % parameters.timeReachConfB = 3; %before timeReachConfB, the search radius grows with time with the power in brownScaling(1); after timeReachConfB it grows with the power in brownScaling(2).
    parameters.timeReachConfB = gapCloseParam.timeWindow; %before timeReachConfB, the search radius grows with time with the power in brownScaling(1); after timeReachConfB it grows with the power in brownScaling(2).

    parameters.ampRatioLimit = []; %for merging and splitting. Minimum and maximum ratios between the intensity of a feature after merging/before splitting and the sum of the intensities of the 2 features that merge/split.

    parameters.lenForClassify = lenForClassify; %minimum track segment length to classify it as linear or random.

    parameters.useLocalDensity = useLocalDensity2; %1 if you want to expand the search radius of isolated features in the gap closing and merging/splitting step.
    parameters.nnWindow = gapCloseParam.timeWindow; %number of frames before/after the current one where you want to look for a track's nearest neighbor at its end/start (in the gap closing step).

    parameters.linStdMult = linStdMult*ones(gapCloseParam.timeWindow,1); %multiplication factor to calculate linear search radius from standard deviation.

    parameters.linScaling = linScaling; %power for scaling the linear search radius with time (similar to brownScaling).
    % parameters.timeReachConfL = 4; %similar to timeReachConfB, but for the linear part of the motion.
    parameters.timeReachConfL = gapCloseParam.timeWindow; %similar to timeReachConfB, but for the linear part of the motion.

    parameters.maxAngleVV = maxAngleVV; %maximum angle between the directions of motion of two tracks that allows linking them (and thus closing a gap). Think of it as the equivalent of a searchRadius but for angles.

    %optional; if not input, 1 will be used (i.e. no penalty)
    parameters.gapPenalty = gapPenalty; %penalty for increasing temporary disappearance time (disappearing for n frames gets a penalty of gapPenalty^(n-1)).

    %optional; to calculate MS search radius
    %if not input, MS search radius will be the same as gap closing search radius
    parameters.resLimit = []; %resolution limit, which is generally equal to 3 * point spread function sigma.

    %NEW PARAMETER
    parameters.gapExcludeMS = gapExcludeMS; %flag to allow gaps to exclude merges and splits

    %NEW PARAMETER
    parameters.strategyBD = strategyBD; %strategy to calculate birth and death cost

    costMatrices(2).parameters = parameters;
    clear parameters

    % Kalman filter function names

    kalmanFunctions.reserveMem  = 'kalmanResMemLM';
    kalmanFunctions.initialize  = 'kalmanInitLinearMotion';
    kalmanFunctions.calcGain    = 'kalmanGainLinearMotion';
    kalmanFunctions.timeReverse = 'kalmanReverseLinearMotion';

    % additional input

    %saveResults
    saveResults.dir = ''; %directory where to save input and output
    saveResults.filename = 'tracksTest.mat'; %name of file where input and output are saved
    % saveResults = 0; %don't save results

    %verbose state
    verbose = 1;

    %problem dimension
    probDim = 2;

    movieInfo = fake_movie_info(position);



    [tracksFinal,kalmanInfoLink,errFlag,costMatrices2] = trackCloseGapsKalmanSparse(movieInfo,...
        costMatrices,gapCloseParam,kalmanFunctions,probDim,saveResults,verbose);

    %figure(2)
    trajectory_matrix = nan(size(position,1),0,2);
    for i = 1:numel(tracksFinal)
        py = tracksFinal(i).tracksCoordAmpCG(:,2:8:end)';
        px = tracksFinal(i).tracksCoordAmpCG(:,1:8:end)';
        s = tracksFinal(i).seqOfEvents(1,1);
        x = 1:size(py,1);
        x = repmat(x+s-1,[size(py,2),1])';
        for j = 1:size(py,2)
            xx = x(:,j);
            ppy = py(:,j);
            ppx = px(:,j);
            soe = tracksFinal(i).seqOfEvents;
            soe(soe(:,2)==1,:);
            splitting = soe(j,4);
            if ~isnan(splitting)
                ppy2 = py(:,soe(j,4));
                ppx2 = px(:,soe(j,4));
                ppy(soe(j,1)-soe(1,1)) = ppy2(soe(j,1)-soe(1,1));
                ppx(soe(j,1)-soe(1,1)) = ppx2(soe(j,1)-soe(1,1));
            end
            trajectory_matrix(:,end+1,:)=nan;
            trajectory_matrix(xx,end,1)=ppy;
            trajectory_matrix(xx,end,2)=ppx;
            %xx = xx(~isnan(ppy));
            %ppy = ppy(~isnan(ppy));
            %plot(xx,ppy)
            %if ~ishold,hold on,end
        end
        
        
    end
    %hold off
    %ylim([-30,30])
    %xlim([0,60])
    %title('uTrack')
    
    %plotTracks2D(tracksFinal)
end

function [movieInfo] = fake_movie_info(position)
    % fake movie info
    moveInfoEmpty = struct('xCoord',zeros(0,2),'yCoord',zeros(0,2),'amp',zeros(0,2),'int',zeros(0,2),'ecc',zeros(0,2));
    range = 1:size(position,1);
    int = [0.0001258,0];
    ecc = [0.7,0];
    amp = [13,0];
    for i = 1:numel(range)
        entry = moveInfoEmpty;
        pp = squeeze(position(range(i),:,:));
        pp = pp(~isnan(pp(:,1)),:);
        entry.xCoord = cat(2,pp(:,2),repmat(0.5,[numel(pp)/2,1]));
        entry.yCoord = cat(2,pp(:,1),repmat(0.5,[numel(pp)/2,1]));
        entry.amp = repmat(amp,[numel(pp)/2,1]);
        entry.ecc = repmat(ecc,[numel(pp)/2,1]);
        entry.int = repmat(int,[numel(pp)/2,1]);
        movieInfo(i,1) = entry;
    end
end
