%% Simulated data

%Parameters
D = 2e-4/0.0668^2*60;%diffusion in pixel^2 per minute
false_negative = 0.0822;%estmiate of false negative density
false_positive = 0.0244;%estimate of false positive density
frame_skips = 3;%number of maximum frame skips
cmax = 0.0001;%this is the probability at which a connection is deemed to improbable to be true and wont be considred further
run_stitching = true;%run the stitching step to connected fragments returned by the A* tracking step 
max_candidate_list_size = 100000;%once the candidate list in the A* tracking step exceeds this number, the worst 10% of candidates are trimmed

%load localizations
point_matrix = load_file("example_data/points_simulated.txt"); 

%plot ground truth
figure(2) 
subplot(1,3,1)
plot(point_matrix(:,:,1),'x')
format_plot("Ground Truth",[0,60])

%generated and plot altered data
[point_matrix_altered] = add_fp_and_fn(point_matrix,20,40);%add 20 fake points and remove 40 real points
subplot(1,3,2)
plot(point_matrix_altered(:,:,1),'x')
format_plot("Altered Data",[0,60])

%track and plot altered data
trajectory_matrix_inferred = track_points(point_matrix_altered,D,false_negative,false_positive,frame_skips,cmax,run_stitching,max_candidate_list_size);%track altered data
subplot(1,3,3)
plot_trajectory_matrix(trajectory_matrix_inferred)
format_plot("Tracked Data",[0,60])

%% functions
function [point_matrix] = add_fp_and_fn(point_matrix,n_add_fake_points,n_remove_real_points)
    
    f = find(~isnan(point_matrix(:,:,1)));
    
    if n_remove_real_points
        r = rand(1,numel(f));
        [~,permutation] = sort(r);
        permutation = permutation(1:n_remove_real_points);% get nfn many random positions and replace them with nan
        point_matrix(f(permutation)) = nan;
        point_matrix(f(permutation)+prod(size(point_matrix,[1,2]))) = nan;
    end
    position_fp = point_matrix;
    position_fp(:) = nan;
    
    
    if n_add_fake_points
        y = rand(1,n_add_fake_points)*60;%random x and y position
        x = rand(1,n_add_fake_points)*8;
        t = round(rand(1,n_add_fake_points)*59)+1;
        for i = 1:n_add_fake_points
            j = 1;
            while true
                if isnan(position_fp(t(i),j,1))
                    position_fp(t(i),j,:) = [y(i),x(i)];
                    break
                else
                    j = j + 1;
                    if j > size(position_fp,2)
                        t(i) = round(rand(1,1)*59)+1;
                        j = 1;
                    end
                end
            end
        end
    end
    
    point_matrix = cat(2,point_matrix,position_fp);
end

function format_plot(name,ylimit)

    xlabel("Frame")
    ylabel("Position (Pixel)")
    ylim(ylimit)
    title(name)
end