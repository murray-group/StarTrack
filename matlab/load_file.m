function [point_matrix] = load_file(path)
    points = readtable(path);
    n_frames = max(points.t);
    n_points_per_frame = 0;
    for it_frame = 1:n_frames
        n_points = sum(points.t==it_frame);
        if n_points>n_points_per_frame,n_points_per_frame = n_points;end
    end
    point_matrix = nan(n_frames,n_points_per_frame,2);
    for it_frame = 1:numel(points.t)
        index = sum(~isnan(point_matrix(points.t(it_frame),:,1)))+1;
        point_matrix(points.t(it_frame),index,:) = [points.x(it_frame),points.y(it_frame)];
    end

end

