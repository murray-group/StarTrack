#include <fstream>
#include <sstream>
#include <array>
#include "data_structure.h"


int main(int argc, char *argv[])
{

    int max_number_of_elements = 1000000;
    floating_point purge_by_that_much = 0.1;
    int check_every = 1;//every time the max_depth is increase by check_every pruge the path list
    std::string path = "cm.csv";
    std::string path_out = "tracked.txt";


    std::cout << argc << std::endl;
    for(int i = 1; i <= argc-1; i+=2)
    {
        std::string parameter_flag = std::string(argv[i]);
        std::cout << parameter_flag << std::endl;
        if(parameter_flag == "-path") {
            path = argv[i+1];
        } else if(parameter_flag == "-elements") {
            max_number_of_elements = std::stoi(argv[i+1]);
        } else if(parameter_flag == "-check") {
            check_every = std::stoi(argv[i+1]);
        } else if(parameter_flag == "-purge") {
            purge_by_that_much = std::stod(argv[i+1]);
        } else if(parameter_flag == "-path_out") {
            path_out = argv[i+1];
        }
    }

    std::string s;
    std::cout << "start tracking" << std::endl;
    std::cout << "using cost matrix located at: "<< path << std::endl;
    std::ifstream in_file;

    //parse input
    in_file.open(path);
    int n_points = 0;
    while (in_file >> s) {//figure out how many lines of text there are
        //n_entries = std::count(s.begin(), s.end(), ',')+1;
        n_points++;
    }
    //std::cout << n_points << std::endl;
    in_file.close();

    in_file.open(path);
    std::vector<std::vector<Edge*>> edges;
    //std::vector<Edge*> edges[n_points];
    std::string entry;
    for (int y=0;in_file >> s;y++) {
        std::stringstream ss(s);
        for(int x=0; ss.good();x++)
        {
            if(x==0){
                for(int z=0; z<n_points;z++){
                    edges.push_back(std::vector<Edge*>());
                }
            }
            getline( ss, entry, ',' );
            if(entry!="NaN") {
                Edge* edge =  new Edge(x,y,std::stod(entry));
                edges[x].push_back(edge);
            }
        }
    }
    in_file.close();


    //tracking
    std::vector<Edge*> track = tracking(edges,n_points,max_number_of_elements,purge_by_that_much,check_every);


    //write output
    std::ofstream output_writer;
    output_writer.open(path_out);

    //print and save tacking
    output_writer << "p1\tp2\n";
    std::cout << "complete track:" << std::endl;
    for (Edge* edge : track) {
        std::cout << *edge << "|";
        output_writer << *edge << "\n";
    }
    std::cout << std::endl;

    output_writer.close();


    return 0;
}
