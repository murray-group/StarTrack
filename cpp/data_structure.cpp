#include "data_structure.h"
//----------------------------------------------------------------------------------------------------------------------------------------------------------Edge
Edge::Edge(int p1, int p2, floating_point cost) : p1(p1), p2(p2), cost(cost)
{
}

std::ostream& operator << ( std::ostream& outs, const Edge& e)
{
  return outs << e.p1+1 << "\t" << e.p2+1;//+1 is need for matlab
}


//----------------------------------------------------------------------------------------------------------------------------------------------------------Path

Path::Path(int depth, int max_depth, floating_point cost_current, floating_point cost_estimate)//path constructor
    : depth(depth), max_depth(max_depth), cost_current(cost_current), cost_estimate(cost_estimate)
{
    for(int i = 0; i < max_depth; i++){
        used.push_back(false);
    }
}

bool Path::is_used(Edge* edge)
{
    return used[edge->p2];
}

void Path::add_edge(Edge* edge, floating_point ce)//add and edge to the path
{
    if(used[edge->p2])//if edge points to a point it is already pointing to throw an error because something went wrong (probably some bug or bad coding on my side)
        throw std::invalid_argument( "path received an edge to an entry it already contains" );
    if(depth!=edge->p1)//if edge points from a point which is it the current depth something went wrong (probably some bug or bad coding on my side)
        throw std::invalid_argument( "path received an edge which points from a different depth" );
    //else add edge to edge vector and label the point the edge points to as used
    edges.push_back(edge);
    cost_current+=edge->cost;
    cost_estimate=ce;
    if(edge->p2<max_depth-2){//with the exception of the edges pointing to the
        used[edge->p2] = true;
    }
    depth++;
}

bool operator <(const Path& x, const int y) {//these two are need to purge the list
    return x.depth < y;
}

bool operator ==(const Path& x, const int y) {//these two are need to purge the list
    return x.depth == y;
}

bool operator <(const Path& x, const Path& y) {//check if the cost is lower, but if cost is equal check the depth
    return x.cost_current + x.cost_estimate < y.cost_current + y.cost_estimate
        || (x.cost_current + x.cost_estimate == y.cost_current + y.cost_estimate && x.depth < y.depth);
}

bool operator <=(const Path& x, const Path& y) {//check if the cost is lower-equal
    return x.cost_current + x.cost_estimate <= y.cost_current + y.cost_estimate;
}

bool operator >(const Path& x, const Path& y) {//analogs to the above
    return x.cost_current + x.cost_estimate > y.cost_current + y.cost_estimate
        || (x.cost_current + x.cost_estimate == y.cost_current + y.cost_estimate && x.depth > y.depth);
}

bool operator >=(const Path& x, const Path& y) {
    return x.cost_current + x.cost_estimate >= y.cost_current + y.cost_estimate;
}

bool operator ==(const Path& x, const Path& y) {//check if cost and depth are equal
    return x.cost_current + x.cost_estimate == y.cost_current + y.cost_estimate && x.depth == y.depth;
}

std::ostream& operator << ( std::ostream& outs, const Path& p)
{
  return outs << p.depth << " " << p.cost_current + p.cost_estimate;
}




//----------------------------------------------------------------------------------------------------------------------------------------------------------Structured_list

Structured_list::Structured_list()
{
    max_depth = 0;
}

void Structured_list::push(Path path)
{
    paths.insert(path);
    if(path.depth>max_depth)
    {
        max_depth = path.depth;
    }
}

void Structured_list::pop()
{
    paths.erase(paths.cbegin());
}

Path Structured_list::top()
{
    return *paths.cbegin();
}

int Structured_list::size()
{
    return paths.size();
}

void Structured_list::purge_list_by_depth(int thresh)//purge list from path with a depth < threshold
{
    for (auto it = paths.begin(), last = paths.end(); it != last; ) {
        if (*it<thresh) {
            it = paths.erase(it);
        } else {
            ++it;
        }
    }
}

void Structured_list::purge_list(floating_point percent,int min_depth,int max_depth)//retain percent paths. Do this by removing the (1-percent) of paths. The paths are chosen by lowest length first and second by cost
{
    //no path in list is allowed to have a lower depth than min_depth and a greater depth than max_depth
    //determine how many paths there are at each depth
    std::vector<int> counts(max_depth-min_depth+1,0);
    for (auto it = paths.begin(); it != paths.end(); ++it){
        counts[it->depth-min_depth]++;
    }


    int n_retain = (int)(paths.size()*percent);//number of elements left in paths are purging
    int thresh = max_depth;

    //figure out which depth is the threshold (lower than this all paths are removed) and how many paths are removed from the threshold depth
    for(int i = max_depth; i >= min_depth;i--){
        if(n_retain <= counts[i-min_depth]){
            thresh = i;
            break;
        }
        n_retain=n_retain-counts[i-min_depth];
    }

    for (auto it = paths.begin(), last = paths.end(); it!=last;) {//pruge list
        if (*it<thresh) {//if a path is low than the threshold remove it
            it = paths.erase(it);
        } else {
            if(*it==thresh){//if a path is at the threshold depth
                if(n_retain>0) {//either keep it based on the number of paths which need to be retained from this depth
                    n_retain--;
                    ++it;
                } else {//or remove it
                    it = paths.erase(it);
                }
            } else {//depth is greater than thresh and therefore path is kept
                ++it;
            }
        }
    }
}




//----------------------------------------------------------------------------------------------------------------------------------------------------------tracking
std::vector<Edge*> tracking(std::vector<std::vector<Edge*>> edges,int n_points,int max_number_of_elements,floating_point purge_by_that_much,int check_every)
{
    floating_point cost_estimate[n_points];//contains the estimate cost to finish the tracking at a specific depth
    floating_point min_cost, cost_last = 0;
    int max_jump = 0;
    for (int y=n_points-1;y>=0;y--) {//create an edge stuct for each possbile connection between two points
        min_cost = 100000000;//find the lowest cost for each point to be connected to another point
        for (Edge* edge : edges[y]) {
            floating_point current_jump = edge->p2-edge->p1;
            if (current_jump > max_jump && edge->p2 < n_points-2)// n_lines - 1 to exclude edges going into the virtual one at the end
                max_jump = current_jump;
            if (edge->cost<min_cost)
                min_cost = edge->cost;
        }
        cost_estimate[y]=min_cost+cost_last;//since we iterate in reverse over n_lines we can simply add the last cost to the current min to do a reverse cumsum
        cost_last = cost_estimate[y];//update the last cost
    }
    //std::cout << max_jump << std::endl;



    Structured_list paths;//create a structured list (sorted data structure) to store all possible paths to traverse the tree of all possible trackings
    for (Edge* edge : edges[0]) {//initiate queue
        Path path{0,n_points,0,cost_estimate[0]};
        path.add_edge(edge,cost_estimate[path.depth+1]);
        paths.push(path);
    }

    int thresh = max_jump;//purge the path list by removing all paths which are thresh-many paths below max_depth
    //int check_every = 1;//every time the max_depth is increase by check_every pruge the path list
    int last_cleaned = 0;//keeps track of last purgining
    int max_depth = 0;
    
    //int max_number_of_elements = 100000;
    //floating_point purge_by_that_much = 0.5;
    for(int i = 1; i < 100000000; i++){//sets a limit of iterations before the tracking is aboarded (can be replaced with while(true) to iterate until the bitter end)
        Path path = paths.top();//get the path with the highest potential

        if(path.depth>max_depth){
            if(path.depth==n_points-2){//if the best path reached the last foci the tracking is finished
                break;
            }
            max_depth = path.depth;
        }

        paths.pop();//remove current path from the list (must be execuded before break condition otherwise the path is lost)

        for (Edge* edge : edges[path.depth]) {//check each possible connection for the current depth
            if(!path.is_used(edge)){//if the point in the possible connection is not used add a new path with the added edge to the path list
                Path duplicate = path;
                duplicate.add_edge(edge,cost_estimate[duplicate.depth+1]);
                paths.push(duplicate);
            }
        }

        if(last_cleaned+check_every<=max_depth) {//pruge list
            paths.purge_list_by_depth(max_depth-thresh);
            last_cleaned+=check_every;
            //std::cout << "cleaned" << std::endl;
        }

        if(paths.size()>max_number_of_elements){
            paths.purge_list(1-purge_by_that_much,last_cleaned-thresh-1,max_depth+1);//+1 in case the one of the paths added has a higher max_depth
        }

        if(i%100000==0) {//show how tracking is going every so often
            std::cout << "iteration:" << i << "\tmax_depth:" << max_depth << "\tnumber of elements in path list:" <<  paths.size() << std::endl;
        }
    }
    std::vector<Edge*> track = paths.top().edges;
    return track;
}

