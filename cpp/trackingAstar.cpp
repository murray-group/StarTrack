/* MyMEXFunction
 * Adds second input to each  
 * element of first input
 * a = MyMEXFunction(a,b);
*/

#include "mex.hpp"
#include "mexAdapter.hpp"
#include "MatlabDataArray.hpp"
#include "data_structure.h"

using namespace matlab::data;
using matlab::mex::ArgumentList;

class MexFunction : public matlab::mex::Function {
public:
    void operator()(ArgumentList outputs, ArgumentList inputs) {
        TypedArray<floating_point> doubleArray = std::move(inputs[0]);
        int n_points = 0;
        for (auto& elem : doubleArray) {
            n_points++;
            
        }
        floating_point cost = 0;
        n_points = std::sqrt(n_points);
        std::vector<std::vector<Edge*>> edges;
        
        for(int x = 0; x < n_points; x++) {
            edges.push_back(std::vector<Edge*>());
            for(int y = 0; y < n_points; y++) {
                cost = doubleArray[y][x];
                if(cost==cost){
                    Edge* edge =  new Edge(x,y,cost);
                    edges[x].push_back(edge);
                }
            }
        }
        
        
        int max_number_of_elements = 1000000;
        floating_point purge_by_that_much = 0.1;
        int check_every = 1;//every time the max_depth is increase by check_every pruge the path list
        
        if (inputs.size() >= 4) {
            TypedArray<floating_point> tmp = std::move(inputs[3]);
            check_every = tmp[0];
        }
        if (inputs.size() >= 3) {
            TypedArray<floating_point> tmp = std::move(inputs[2]);
            purge_by_that_much = tmp[0];
        }
        if (inputs.size() >= 2) {
            TypedArray<floating_point> tmp = std::move(inputs[1]);
            max_number_of_elements = tmp[0];
        }
        
        
        std::vector<Edge*> track = tracking(edges,n_points,max_number_of_elements,purge_by_that_much,check_every);
        std::vector<floating_point> from, to;
        for(Edge* edge:track)
        {
            from.push_back(edge->p1+1);//plus one for the conversion to matlab indices
            to.push_back(edge->p2+1);
        }
        long unsigned int length_output = from.size();
        from.insert( from.end(), to.begin(), to.end() );

        // Create a TypedArray
        ArrayFactory factory;
        TypedArray<floating_point>  output = factory.createArray( {length_output,2}, from.begin(), from.end()); 
        
        
        outputs[0] = output;
    }

};