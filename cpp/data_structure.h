#include <iostream>
#include <unistd.h>
#include <queue>
#include <set>


typedef double floating_point;


struct Edge {
    int p1;//point from which the edge comes from
    int p2;//point to which the edge is pointing to
    floating_point cost;//cost of the edge

    Edge(int p1, int p2, floating_point cost);
};

std::ostream& operator << ( std::ostream& outs, const Edge& e);

struct Path {
    floating_point cost_estimate;//cost estimate of how much itll cost to finish the tracking
    floating_point cost_current;//the cost associated with the current tracking (the part that is already determined)
    int depth;//depth of the current path
    int max_depth;//if max_depth is reached we are done (number of points)
    std::vector<bool> used;//used up points (points which are pointed to)
    std::vector<Edge*> edges;//all edges in the current tracking

    Path(int depth, int max_depth, floating_point cost_current, floating_point cost_estimate);
    bool is_used(Edge* edge);//check if point to which edge points is already part of the tracking
    void add_edge(Edge* edge, floating_point ce);//add and edge to the path
};

std::ostream& operator << ( std::ostream& outs, const Path& p);

//Data structure containing an ordered set which is used to keep track of all the paths
class Structured_list {
public:
    std::set<Path> paths;
    int max_depth;

    Structured_list();
    void push(Path path);
    void pop();
    Path top();
    int size();
    void purge_list_by_depth(int thresh);
    void purge_list(floating_point percent,int min_depth,int max_depth);
};

std::vector<Edge*> tracking(std::vector<std::vector<Edge*>>,int n_points,int max_number_of_elements,floating_point purge_by_that_much,int check_every);
/*
*/
