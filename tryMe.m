%% Track Plamid localisations

%Parameters
D = 2e-4/0.0668^2*60;%diffusion in pixel^2 per minute
false_negative = 0.0822;%estmiate of false negative density
false_positive = 0.0244;%estimate of false positive density
frame_skips = 3;%number of maximum frame skips
cmax = 0.0001;%this is the probability at which a connection is deemed to improbable to be true and wont be considred further
run_stitching = true;%run the stitching step to connected fragments returned by the A* tracking step 
max_candidate_list_size = 100000;%once the candidate list in the A* tracking step exceeds this number, the worst 10% of candidates are trimmed

%load localizations
localisation_matrix = load_file("example_data/points.txt");

%plot raw data
figure(1) 
subplot(1,2,1)
plot(localisation_matrix(:,:,1),'x')
xlabel("Frame")
ylabel("Position (Pixel)")
ylim([0,70])
title("Raw Data")

%track and plot raw data
[trajectory_matrix,edges,fd] = track_points(localisation_matrix,D,false_negative,false_positive,frame_skips,cmax,run_stitching,max_candidate_list_size);%track altered data
subplot(1,2,2)
plot_trajectory_matrix(trajectory_matrix)
xlabel("Frame")
ylabel("Position (Pixel)")
ylim([0,70])
title("Tracked Data")
