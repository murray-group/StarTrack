# *Track
Fluorescent microscopy is the primary method to study DNA organization within cells. However the variability and low signal-to-noise commonly associated with live-cell time lapse imaging challenges quantitative measurements. In particular, obtaining quantitative or mechanistic insight often depends on the accurate tracking of fluorescent particles. Here, we present *Track, an inference method that determines the most likely temporal tracking of replicating intracellular particles such DNA loci while accounting for missing, merged and spurious detections. It allows the accurate prediction of particle copy numbers as well as the timing of replication events. 
## Parameters
*Track is a tool for the accurate tracking of replicating chromosomal and extrachromosomal DNA, and other low-copy number persistent particles, from time lapse images of live cells. The algorithm is transparent and user friendly and is completely specified, in the default case, by only four input parameters (there are no hidden internal parameters):

- Diffusion Coefficient
- False positive probability (probability that a detected spot is not real)
- False negative probability (probability that a real spot is not detected)
- Number of allowed frame skips
- cmax (optional, threshold on the probability of linking two spots)
- Max candidate list size (the max size of the candidate list in the A*-tracking step. This parameter influences runtime and memory consumption)

## Setup
The core of the algorithm is implemented in c++. Matlab and Python implementations are provided in this repository so that *Track can be easily integrated into existing spot detection pipelines:

- Matlab 
	- tryMe.m 
	- cpp/run_tracking_via_cl.m (this script executes the c++ code by command line and not from a .mex file)
-	Python:
	- python/run_tracking.py
	
### matlab
The easiest way to run *Track is from matlab. This can be done as follws: 

1. Add the StarTrack to the path (right click the folder on the "Current Folder" panel inside matlab)
2. Opitonally you can recomplie the c++ code into a mex file step (3-5)
3. Navigate to the cpp folder indside the StarTrack folder
4. In the "Command Window" and type: mex -setup c++ (set up complier as instructed by matlab if needed)
5. Afterwards type: mex -R2018a trackingAstar.cpp data_structure.cpp (The existing .mex file will be overwritten, trackingAstar.mexa64 / trackingAstar.mexw64 for linux or windows respectively)
6. Navigate into the StarTrack folder
7. Open tryMe.m and execute the script 

### python
TBD

## Input format
The input format is a simlpe text file with three columns seperated by tab. The first column contains the x position, the second the y position and the last the time point/frame. the first row is a header which describes the content of each column. For example: 

| x | y | t |
| ------ | ------ | ------ |
| 13.2 | 4.3 | 1 |
| 23.2 | 7.3 | 1 |
| 28.4 | 8.6 | 1 |
| 20.9 | 7.5 | 2 |
| 32.3 | 9.9 | 2 |

## Output format
### matlab
Running *Track creates a 3D matrix, called trajectory_matrix, containing all individual trajectories of the finished tracking. This matrix contains all localizations that are part of the tracking. The first dimension of the matrix defines the frame, the second dimension defines to which trajectory the localisation belongs to, and the third dimension defines the spatial component of the localisation. For example:

- TrajectoryMatrix(5,4,1) -> The 1st spatial component (x) of the 4th trajectory on the 5th frame
- TrajectoryMatrix(20,7,2) -> The 2nd spatial component (y) of the 7th trajectory on the 20th frame

When there are frames skips within a trajectory, they are represented as NaNs.
## Publication
More information can be found here in our paper

Robin Köhler, Ismath Sadhir, Seán M. Murray,
★Track: Inferred counting and tracking of replicating DNA loci,
Biophysical Journal, Volume 122, Issue 9, 2023,

https://doi.org/10.1016/j.bpj.2023.03.033

(preprint at https://www.biorxiv.org/content/10.1101/2022.12.05.519146v1)
